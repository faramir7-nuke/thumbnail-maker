"""
Author: Mathieu Devos
File: magnetNodes.py
Date: 2022.12.09
Revision: 2022.12.09
Copyright: Copyright 2022 Mathieu Devos

           Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
           documentation files (the "Software"), to deal in the Software without restriction, including without
           limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
           the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
           conditions:

           The above copyright notice and this permission notice shall be included in all copies or substantial
           portions of the Software.

           THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
           TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
           SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
           ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
           OR OTHER DEALINGS IN THE SOFTWARE.

Brief: A small tool that automatically creates a "thumbnail" for a breakdown of the compositing.

"""

__author__ = 'Mathieu Devos <https://www.linkedin.com/in/mathieu-devos/>'
__copyright__ = 'Copyright 2022, Mathieu Devos'
__credits__ = []
__license__ = 'MIT License'
__maintainer__ = 'Mathieu Devos'
__email__ = 'mathieu.devos@free.fr'
__status__ = 'Testing'


"""Here starts the code"""

def thumbnail():
    nodes = nuke.selectedNodes()
    if len(nodes) != 2:
        nuke.message('Please select exactly two nodes: first the "before comp" node and then the "after comp" node.')
        return None
    before_node, after_node = nodes[1], nodes[0]
    height, width = before_node.height(), before_node.width()
    save_location = nuke.getFilename('Where should I save the thumbnail? Please provide the filename with an extension (e.g. "Satoru Umezawa.png" or "Colossal Dreadmaw.jpg")')
    current_frame = nuke.frame()

    # FIRST MERGE NODE
    merge_over_node = nuke.nodes.Merge()
    merge_over_node.connectInput(0, before_node)

    # SECOND MERGE NODE
    merge_mask_node = nuke.nodes.Merge()
    merge_mask_node['operation'].setValue('mask')
    merge_mask_node.connectInput(0, after_node)
    merge_over_node.connectInput(1, merge_mask_node)

    # RAMP (TO SPLIT THE FRAME IN HALF)
    ramp_node = nuke.nodes.Ramp()
    ramp_node['p0'].setValue([width/2-.0001,height/2])
    ramp_node['p1'].setValue([width/2,height/2])
    merge_mask_node.connectInput(1, ramp_node)

    # SOME TEXT
    artist_text_node = nuke.nodes.Text2()
    artist_name = nuke.getInput('Artist name?', 'Mathieu DEVOS')
    artist_text_node['message'].setValue('Artist: %s' % artist_name)
    artist_text_node['box'].setValue([width/2 - .3*width,17*height/20 - .3*height,width/2 + .3*width,17*height/20 + .3*height])
    artist_text_node['xjustify'].setValue('center')
    artist_text_node['yjustify'].setValue('center')
    artist_text_node['global_font_scale'].setValue(.3)
    artist_text_node['enable_shadows'].setValue('enabled')
    artist_text_node['shadow_opacity'].setValue(1)
    artist_text_node['shadow_angle'].setValue(315)
    artist_text_node['shadow_distance'].setValue(5)
    artist_text_node.connectInput(0, merge_over_node)

    artwork_text_node = nuke.nodes.Text2()
    artwork_name = nuke.getInput('Artwork name?', 'My artwork')
    artwork_text_node['message'].setValue(artwork_name)
    artwork_text_node['box'].setValue([width/2 - .3*width,9*height/10 - .3*height,width/2 + .3*width,9*height/10 + .3*height])
    artwork_text_node['xjustify'].setValue('center')
    artwork_text_node['yjustify'].setValue('center')
    artwork_text_node['global_font_scale'].setValue(.7)
    artwork_text_node['enable_shadows'].setValue('enabled')
    artwork_text_node['shadow_opacity'].setValue(1)
    artwork_text_node['shadow_angle'].setValue(315)
    artwork_text_node['shadow_distance'].setValue(5)
    artwork_text_node.connectInput(0, artist_text_node)

    before_text_node = nuke.nodes.Text2()
    before_text_node['message'].setValue('BEFORE COMP')
    before_text_node['box'].setValue([width/4 - .3*width,height/2 - .3*height,width/4 + .3*width,height/2 + .3*height])
    before_text_node['xjustify'].setValue('center')
    before_text_node['yjustify'].setValue('center')
    before_text_node['global_font_scale'].setValue(.7)
    before_text_node['enable_shadows'].setValue('enabled')
    before_text_node['shadow_opacity'].setValue(1)
    before_text_node['shadow_angle'].setValue(315)
    before_text_node['shadow_distance'].setValue(5)
    before_text_node.connectInput(0, artwork_text_node)

    after_text_node = nuke.nodes.Text2()
    after_text_node['message'].setValue('AFTER COMP')
    after_text_node['box'].setValue([3*width/4 - .3*width,height/2 - .3*height,3*width/4 + .3*width,height/2 + .3*height])
    after_text_node['xjustify'].setValue('center')
    after_text_node['yjustify'].setValue('center')
    after_text_node['global_font_scale'].setValue(.7)
    after_text_node['enable_shadows'].setValue('enabled')
    after_text_node['shadow_opacity'].setValue(1)
    after_text_node['shadow_angle'].setValue(315)
    after_text_node['shadow_distance'].setValue(5)
    after_text_node.connectInput(0, before_text_node)

    from datetime import date
    date_text_node = nuke.nodes.Text2()
    date_text_node['message'].setValue(date.today().strftime("%B %d, %Y"))
    date_text_node['box'].setValue([12*width/13 - .6*width,height/13,12*width/13,height/13 + .6*height])
    date_text_node['xjustify'].setValue('right')
    date_text_node['yjustify'].setValue('bottom')
    date_text_node['global_font_scale'].setValue(.3)
    date_text_node['enable_shadows'].setValue('enabled')
    date_text_node['shadow_opacity'].setValue(1)
    date_text_node['shadow_angle'].setValue(315)
    date_text_node['shadow_distance'].setValue(5)
    date_text_node.connectInput(0, after_text_node)

    # WRITE NODE
    write_node = nuke.nodes.Write()
    write_node['file'].setValue(save_location)
    write_node.connectInput(0, date_text_node)
    nuke.render(write_node, current_frame, current_frame)

    # DELETING THE NODES WE JUST CREATED
    nodes_to_delete = [merge_over_node, merge_mask_node, ramp_node, artist_text_node, artwork_text_node, before_text_node, after_text_node, date_text_node, write_node]
    for node in nodes_to_delete:
        nuke.delete(node)

    
nuke.menu('Nuke').addCommand('LittleHelpers/ThumbnailMaker', 'thumbnail()')
