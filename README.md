# Thumbnail Maker

The goal of this small tool it to automatically create a "thumbnail" for a breakdown of the compositing.

Example of thumbnail:

![thumbnail example](https://gitlab.com/faramir7-nuke/thumbnail-maker/-/raw/main/thumbnail_example.png)

I pushed the lens flares a bit much here, you can find the real version on my ArtStation: https://www.artstation.com/artwork/DA4Nve

It has been tested successfully in **Nuke 13.2v5.**
